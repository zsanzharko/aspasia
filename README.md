# Aspasia
### A family of projects that interact with each other.

This huge project was created for home use. It is understood that there will be
a home server at home, which will be ready to process requests coming from different devices.

This huge project was created for home use. It is understood that there will be a home server at home, which will be
ready to process requests coming from different devices. The project will consist of the main components as a back-end
and an information viewing tool on the front-end. It is assumed that there will be an application

Technologies and stacks used in the project:
- Java 17
- Spring Boot
- Spring Web
- Spring Data
- Spring JPA
- Dart
- Flutter

  **_Soon_**
- Html, CSS, JavaScript
- C, C++
- Python
- ML, DL
- Computer Vision
- Natural Language Processing

The main objective of this project is the automation of physical components that interact with the outside world.
That is, when developing a robot that will perform a number of tasks, it will be sent to the server for further data 
processing. Depending on the type of device that will be implemented, it will be possible to manage data through the 
main component (Server)

## Getting Started