package kz.aspasia.aspasiabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AspasiaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AspasiaBackendApplication.class, args);
	}

}
