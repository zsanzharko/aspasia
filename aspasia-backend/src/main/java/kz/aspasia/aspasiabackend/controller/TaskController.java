package kz.aspasia.aspasiabackend.controller;

import kz.aspasia.aspasiabackend.dto.TaskDto;
import kz.aspasia.aspasiabackend.entity.Task;
import kz.aspasia.aspasiabackend.exception.ValidationException;
import kz.aspasia.aspasiabackend.service.TaskService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/tasks")
@AllArgsConstructor
@Log
@CrossOrigin
public class TaskController {

    private final TaskService service;

    @PostMapping("/save")
    public Task saveTask(@RequestBody TaskDto dto) throws ValidationException {
        log.info("Handling save a task");
        return service.saveTask(dto);
    }

    @GetMapping("/findAll")
    public List<TaskDto> findAllTasks() {
        log.info("Handling find all tasks request");
        return service.findAll();
    }
}
