package kz.aspasia.aspasiabackend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto implements Serializable {

    private Integer task_id;
    private String title;
    private String description;
    private boolean isDone = false;
}
