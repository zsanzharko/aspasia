package kz.aspasia.aspasiabackend.entity;

import kz.aspasia.aspasiabackend.entity.abstractEntity.AbstractList;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "task_list")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Task extends AbstractList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id", nullable = false)
    private Integer task_id;

    @Column(name = "is_done")
    private boolean isDone;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Task task = (Task) o;
        return task_id != null && Objects.equals(task_id, task.task_id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
