package kz.aspasia.aspasiabackend.entity.abstractEntity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public abstract class AbstractList {

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;
}
