package kz.aspasia.aspasiabackend.repository;

import kz.aspasia.aspasiabackend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findUserByName(String name);

    User findUserByEmail(String email);

    User findUserByUsername(String username);
}
