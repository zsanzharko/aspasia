package kz.aspasia.aspasiabackend.service;

import kz.aspasia.aspasiabackend.dto.UserDto;
import kz.aspasia.aspasiabackend.entity.User;
import kz.aspasia.aspasiabackend.exception.ValidationException;
import kz.aspasia.aspasiabackend.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;


@Service
@AllArgsConstructor
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;
    private final UserConverter userConverter;

    @Override
    public UserDto saveUser(UserDto userDto) throws ValidationException {
        validateUserDto(userDto);
        User user = userRepository.save(userConverter.fromUserDtoToUser(userDto));

        return userConverter.fromUserToUserDto(user);
    }

    @Override
    public void deleteUser(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public UserDto findByUsername(String username) {
        return userConverter.fromUserToUserDto(userRepository.findUserByUsername(username));
    }

    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll()
                .stream()
                .map(userConverter::fromUserToUserDto)
                .collect(Collectors.toList());
    }

    private void validateUserDto(UserDto userDto) throws ValidationException {

        if (isNull(userDto)) {
            throw new ValidationException("Object user is null");
        }

        if (isNull(userDto.getUsername()) || userDto.getUsername().isEmpty()) {
            throw new ValidationException("User login is empty");
        }


    }
}
