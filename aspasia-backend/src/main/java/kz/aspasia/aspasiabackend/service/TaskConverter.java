package kz.aspasia.aspasiabackend.service;

import kz.aspasia.aspasiabackend.dto.TaskDto;
import kz.aspasia.aspasiabackend.entity.Task;
import org.springframework.stereotype.Component;

@Component
public class TaskConverter {

    public Task fromToDoListDtoToTask(TaskDto dto) {
        Task task = new Task();
        task.setTask_id(dto.getTask_id());
        task.setTitle(dto.getTitle());
        task.setDescription(dto.getDescription());
        task.setDone(dto.isDone());
        return task;
    }

    public TaskDto fromTaskToTaskDto(Task task) {
        return TaskDto.builder()
                .task_id(task.getTask_id())
                .title(task.getTitle())
                .isDone(task.isDone())
                .build();
    }
}
