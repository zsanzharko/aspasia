package kz.aspasia.aspasiabackend.service;

import kz.aspasia.aspasiabackend.dto.TaskDto;
import kz.aspasia.aspasiabackend.entity.Task;
import kz.aspasia.aspasiabackend.exception.ValidationException;
import kz.aspasia.aspasiabackend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
@Transactional
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskConverter taskConverter;

    @Autowired
    public TaskService(TaskRepository taskRepository, TaskConverter taskConverter) {
        this.taskRepository = taskRepository;
        this.taskConverter = taskConverter;
    }

    public Task saveTask(TaskDto taskDto) throws ValidationException {
        validateTaskDto(taskDto);
        Task task = taskConverter.fromToDoListDtoToTask(taskDto);
        return taskRepository.save(task);
    }

    public void deleteTask(Integer task_id){
        taskRepository.deleteById(task_id);
    }

    public TaskDto findById(Integer id) {
        Task task = taskRepository.getById(id);
        return taskConverter.fromTaskToTaskDto(task);
    }

    public List<TaskDto> findAll() {
        return taskRepository.findAll()
                .stream()
                .map(taskConverter :: fromTaskToTaskDto)
                .collect(Collectors.toList());
    }

    public List<TaskDto> findAllByUser_Id(Integer userId) {
        return null;
    }

    private void validateTaskDto(TaskDto taskDto) throws ValidationException {
        if (isNull(taskDto))
            throw new ValidationException("Object task is null");

        if (isNull(taskDto.getTitle()) || taskDto.getTitle().isEmpty())
            throw  new ValidationException("Task title is empty");
    }
}
