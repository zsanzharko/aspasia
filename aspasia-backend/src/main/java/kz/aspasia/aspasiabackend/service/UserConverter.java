package kz.aspasia.aspasiabackend.service;

import kz.aspasia.aspasiabackend.dto.UserDto;
import kz.aspasia.aspasiabackend.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    public User fromUserDtoToUser(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setEmail(userDto.getEmail());
        user.setName(userDto.getName());
        user.setUsername(userDto.getUsername());
        return user;
    }

    public UserDto fromUserToUserDto(User users) {
        return UserDto.builder()
                .id(users.getId())
                .email(users.getEmail())
                .username(users.getUsername())
                .name(users.getName())
                .surname(users.getSurname())
                .build();
    }
}