package kz.aspasia.aspasiabackend.service;

import kz.aspasia.aspasiabackend.dto.UserDto;
import kz.aspasia.aspasiabackend.exception.ValidationException;

import java.util.List;

public interface UserService {

    UserDto saveUser(UserDto userDto) throws ValidationException;

    void deleteUser(Integer userId);

    UserDto findByUsername(String login);

    List<UserDto> findAll();
}
