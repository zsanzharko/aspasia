package kz.aspasia.aspasiabackend.service;


import jdk.jfr.Name;
import kz.aspasia.aspasiabackend.dto.UserDto;
import kz.aspasia.aspasiabackend.entity.User;
import kz.aspasia.aspasiabackend.exception.ValidationException;
import kz.aspasia.aspasiabackend.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DefaultUserServiceTest {

    private UserService userService;
    private UserRepository userRepository;
    private UserConverter userConverter;

    private User user;

    @BeforeEach
    void setUp() {
        userRepository = mock(UserRepository.class);
        userConverter = new UserConverter();
        user = new User();
        user.setName("Sanzhar");
        user.setUsername("sanzharrko");
        user.setId(1);
        when(userRepository.save(any())).thenReturn(user);
        userService = new DefaultUserService(userRepository, userConverter);
    }

    @Test
    @Name("save user return dto")
    void saveUser() throws ValidationException {
        UserDto userDto = UserDto.builder().username("sanzharrko").build();
        UserDto savedUserDto = userService.saveUser(userDto);
        assertThat(savedUserDto).isNotNull();
        assertThat(savedUserDto.getUsername()).isEqualTo("sanzharrko");
    }
}